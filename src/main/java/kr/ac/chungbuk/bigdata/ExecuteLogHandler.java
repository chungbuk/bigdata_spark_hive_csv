package kr.ac.chungbuk.bigdata;

import org.apache.commons.exec.LogOutputStream;
import org.apache.log4j.Level;

import org.apache.log4j.Logger;

/**
 * Created by PENHCHET on 11/20/2016.
 */
public class ExecuteLogHandler extends LogOutputStream{

    private Logger log;

    public ExecuteLogHandler(Logger log, Level level){
        super(level.toInt());
        this.log = log;
    }

    @Override
    protected void processLine(String line, int level) {
        log.log(Level.toLevel(level), line);
    }
}
