package kr.ac.chungbuk.bigdata;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.io.ByteArrayOutputStream;

/**
 * Created by PENHCHET on 11/20/2016.
 */
public class SparkToCSV {

    private Logger logger = Logger.getLogger(getClass());
    private static final String HADOOP_PATH = "/home/hadoop/hadoop/bin/hadoop";
    private static final String HADOOP_URL = "hdfs://namenode:8020";

    //TODO: To save to CSV File from Spark SQL Context with databricks spark csv
    public void startExportWithSqlAndLocation(String strSql, String strSaveLocation){

        System.out.print("String the Apache Spark with Hive to Export the Data to the CSV File ...");
        //TODO: To create the Spark Configuration
        SparkConf sparkConf = new SparkConf(true)
                .setAppName("SparkSQL CSV Integration")
                .setMaster("spark://192.168.0.122:7077");
        sparkConf.set("javax.jdo.option.ConnectionURL", "jdbc:mysql://was.bigdatacenter.org:3306/hivedb?createDatabaseIfNotExist=true");
        sparkConf.set("javax.jdo.option.ConnectionPassword","Dbnis3258!@#$");
        sparkConf.set("javax.jdo.option.ConnectionUserName","hiveuser");
        sparkConf.set("javax.jdo.option.ConnectionDriverName","com.mysql.jdbc.Driver");
        sparkConf.set("hive.metastore.warehouse.dir", "hdfs://192.168.0.122:8020/user/hive/warehouse");
        sparkConf.set("spark.jars.package", "com.databricks.spark-csv_2.10:1.4.0");
        sparkConf.set("spark.executor.memory", "16g");
        sparkConf.set("spark.serializer","org.apache.spark.serializer.KryoSerializer");

        //TODO: TO create or get the SparkSession with Spark Configuration and Enable the Hive Support
        SparkSession sparkSession = SparkSession
                .builder()
                .config(sparkConf)
                .enableHiveSupport()
                .getOrCreate();

        //TODO: To loading Data From HiveQL
        Dataset<Row> dataFrame = sparkSession.sql(strSql);

        //TODO: To show the data Frame
        dataFrame.show();

        try {
            //TODO: To save the dataFrame to the csv file location in the hadoop directory with column header
            dataFrame.repartition(1)
                    .write()
                    .mode(SaveMode.Overwrite)
                    .format("com.databricks.spark.csv")
                    .option("header", "true")
                    .option("codec", "org.apache.hadoop.io.compress.GzipCodec")
                    //TODO: To save the location in the hadoop cluster
                    .save(HADOOP_URL + strSaveLocation);

            //TODO: To clear the Spark Active Session
            System.out.print("Finished the Apache Spark with Hive to Export the Data to the CSV File ...");
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            sparkSession.stop();
            SparkSession.clearActiveSession();
        }

//        //TODO: To copy to Local Directory
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        PumpStreamHandler pumpStreamHandler = new PumpStreamHandler(new ExecuteLogHandler(logger, Level.DEBUG), new ExecuteLogHandler(logger, Level.ERROR));
//        DefaultExecutor executor = new DefaultExecutor();
//        executor.setStreamHandler(pumpStreamHandler);
//
//        String HADOOP_PATH = "/home/hadoop/hadoop/bin/hadoop";
        try {
            String command = HADOOP_PATH + " dfs -copyToLocal " + strSaveLocation + " /home/hadoop/";

            System.out.println("COMMAND ==> " + command);
            //
            //        //TODO: To parse command and return the CommandLine
            //        CommandLine commandLine = CommandLine.parse(command);
            //        try{
            //            //TODO: To execute the CommandLine
            //            executor.execute(commandLine);
            //            System.out.println("Finished Copying File from Hadoop to Linux");
            //        }catch (Exception ex){
            //            ex.printStackTrace();
            //        }

            CommandLineExecutor commandLineExecutor = new CommandLineExecutor(logger);
            commandLineExecutor.execute(command);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
