package kr.ac.chungbuk.bigdata;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;

/**
 * Created by PENHCHET on 11/20/2016.
 */
public class CommandLineExecutor {

    private PumpStreamHandler pumpStreamHandler;
    private DefaultExecutor executor = new DefaultExecutor();
    private CommandLine commandLine;

    public CommandLineExecutor(Logger logger){
        pumpStreamHandler = new PumpStreamHandler(new ExecuteLogHandler(logger, Level.DEBUG), new ExecuteLogHandler(logger, Level.ERROR));
        executor.setStreamHandler(pumpStreamHandler);
    }

    public void execute(String command){
        commandLine = CommandLine.parse(command);
        try{
            //TODO: To execute the CommandLine
            executor.execute(commandLine);
            System.out.println("Finished Copying File from Hadoop to Linux");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
